<?php

declare(strict_types=1);

namespace Shared\ServerSide\Client;

use Shared\ServerSide\HttpClient\InseeAccessTokenHttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class GetInseeTokenClient
{
    private const DAY_TTL = 86400;

    public function __construct(
        protected readonly HttpClientInterface $client,
        private readonly string $consumerKey,
        private readonly string $consumerSecret
    ) {}

    public function __invoke(): string
    {
        $response = $this->client->request('POST', '/token', [
            'body' => ['grant_type' => 'client_credentials'],
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode("{$this->consumerKey}:{$this->consumerSecret}"),
            ],
        ])->toArray();

        $accessToken = $response['access_token'];

        return $accessToken;
    }
}
