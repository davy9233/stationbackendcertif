<?php

declare(strict_types=1);

namespace Shared\ServerSide\Client;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

trait ClientTrait
{
    public function __construct(private readonly HttpClientInterface $client)
    {
    }

    public function __call(string $method, array $args): ?object
    {
        if (\count($args) < 1) {
            throw new \InvalidArgumentException('URI is required for api request.');
        }

        if (!\in_array($method, ['head', 'get', 'put', 'post', 'patch', 'delete'], true)) {
            throw new \RuntimeException('Invalid request method.');
        }

        return $this->request(strtoupper($method), $args[0], $args[1] ?? [], $args[2] ?? null);
    }

    private function request(string $method, string $url, array $options = [], ?string $type = null): ?object
    {
        $result = $this->client->request($method, $url, $options);

        return $result;
    }


}
