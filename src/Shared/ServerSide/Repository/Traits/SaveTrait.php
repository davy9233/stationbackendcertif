<?php

declare(strict_types=1);

namespace Shared\ServerSide\Repository\Traits;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @throws Exception|NotFoundHttpException
 * */
trait SaveTrait
{
    public function save(object $object, bool $canFlush = true): object
    {
        try {
            $this->getEntityManager()->persist($object);
            if ($canFlush) {
                $this->getEntityManager()->flush();
            }
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new NotFoundHttpException('errors.notFound', null, 404);
        }

        return $object;
    }

    /** @return EntityManagerInterface */
    abstract protected function getEntityManager();
}
