<?php

declare(strict_types=1);

namespace Shared\ServerSide\Restructurator;

interface RestructuratorInterface
{
    public function restructure(array $data): array;
}
