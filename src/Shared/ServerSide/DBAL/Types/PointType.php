<?php

declare(strict_types=1);

namespace Shared\ServerSide\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Shared\Domain\Types\Point;

final class PointType extends Type
{
    private const POINT = 'point';

    /** @return string */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform)
    {
        return 'POINT';
    }

    /** @return string */
    public function getName()
    {
        return self::POINT;
    }

    /** @return Point */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        [$longitude, $latitude] = sscanf($value, '(%f, %f)');

        return new Point($latitude, $longitude);
    }

    /**
     * @param Point $value
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->getLongitude() . ',' . $value->getLatitude();
    }

    /**
     * @param string $sqlExpr
     * @return string
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
    {
        return sprintf('PointFromText(%s)', $sqlExpr);
    }
}
