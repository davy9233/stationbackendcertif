<?php

declare(strict_types=1);

namespace Shared\ServerSide\HttpClient;

use Shared\ServerSide\Client\GetInseeTokenClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;

final class InseeAccessTokenHttpClient implements HttpClientInterface
{
    public function __construct(
        protected HttpClientInterface $client,
        private readonly GetInseeTokenClient $getInseeTokenClient
    ) {}

    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        return $this->client->request($method, $url, array_merge_recursive($options, $this->getAuthToken()));
    }

    public function stream($responses, float $timeout = null): ResponseStreamInterface
    {
        return $this->client->stream($responses, $timeout);
    }

    public function withOptions(array $options): static
    {
        $clone = clone $this;
        $clone->client = $this->client->withOptions($options);

        return $clone;
    }

    private function getAuthToken(): array
    {
        /** @var string $token */
        $token = ($this->getInseeTokenClient)();

        return [
            'headers' => [
                'Authorization' => "Bearer {$token}",
            ],
        ];
    }
}
