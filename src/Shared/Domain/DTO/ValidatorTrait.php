<?php

declare(strict_types=1);

namespace Shared\Domain\DTO;

use Assert\LazyAssertion;

trait ValidatorTrait
{
    public function validate(): void
    {
        $this->assertRules()->verifyNow();
    }

    /**
     * Méthode à implémenter pour récupérer les règles à valider
     */
    // abstract private function assertRules(): LazyAssertion;
}
