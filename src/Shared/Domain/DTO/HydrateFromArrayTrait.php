<?php

declare(strict_types=1);

namespace Shared\Domain\DTO;

trait HydrateFromArrayTrait
{
    /**
     * Transforme un tableau associatif en DTO
     */
    final public function hydrateFromArray(array $data): static
    {
        foreach ($data as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method))
                $this->{$method}($value);
        }

        return $this;
    }
}
