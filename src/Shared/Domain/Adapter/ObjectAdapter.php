<?php

declare(strict_types=1);

namespace Shared\Domain\Adapter;

use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\BackedEnumNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

final class ObjectAdapter
{
    private static ?Serializer $serializer = null;

    public static function fromArray(string $to, array $from): object
    {
        self::setSerializer();

        return self::$serializer->denormalize($from, $to);
    }

    public static function objectToObject(string $to, ?array $remapping = null, Object ...$froms): object
    {
        self::setSerializer();

        $data = self::remappingArrayKeys(
            $remapping ?? [],
            self::toArray(...$froms)
        );

        return self::$serializer->denormalize($data, $to);
    }

    public static function toViewData(object $object): array
    {
        return ['data' => self::toArray($object)];
    }

    public static function toArray(object ...$objects): array
    {
        self::setSerializer();

        return array_merge_recursive(
            ...array_map(
                fn (object $object): array => self::$serializer->normalize($object),
                $objects
            )
        );
    }

    private static function instantiateSerializer(): Serializer
    {
        return new Serializer(
            [
                new BackedEnumNormalizer(),
                new PropertyNormalizer(null, null, new ReflectionExtractor()),
                new ArrayDenormalizer(),
                new DateTimeNormalizer(),
                new ObjectNormalizer()
            ],
            [new JsonEncoder()]
        );
    }

    private static function setSerializer(): void
    {
        if (self::$serializer === null)
            self::$serializer = self::instantiateSerializer();
    }

    private static function remappingArrayKeys(array $remapping, array $data): array
    {
        foreach ($remapping as $from => $to) {
            if (isset($data[$from]) === false)
                continue;

            if (is_array($to) && is_array($data[$from]) && self::arrayIsSequential($data[$from]))
                $data[$from] = array_map(fn (array $f): array => self::remappingArrayKeys($to, $f), $data[$from]);
            elseif (is_array($to) && is_array($data[$from]))
                $data[$from] = self::remappingArrayKeys($to, $data[$from]);

            if (is_array($to))
                continue;

            $data[$to] = $data[$from];
            unset($data[$from]);
        }

        return $data;
    }

    /**
     * @see https://stackoverflow.com/a/4254008
     */
    private static function arrayIsSequential(array $array): bool
    {
        return count(array_filter(array_keys($array), 'is_string')) === 0;
    }
}
