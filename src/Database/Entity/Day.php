<?php

namespace Database\Entity;

use App\Repository\DayRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DayRepository::class)]
class Day
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id ;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'day', targetEntity: HoursOpening::class)]
    private Collection $hoursOpenings;

    #[ORM\Column(type:'integer')]
    private $idDay;

    public function __construct()
    {
        $this->hoursOpenings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, HoursOpening>
     */
    public function getHoursOpenings(): Collection
    {
        return $this->hoursOpenings;
    }

    public function addHoursOpening(HoursOpening $hoursOpening): self
    {
        if (!$this->hoursOpenings->contains($hoursOpening)) {
            $this->hoursOpenings->add($hoursOpening);
            $hoursOpening->setDay($this);
        }

        return $this;
    }

    public function removeHoursOpening(HoursOpening $hoursOpening): self
    {
        if ($this->hoursOpenings->removeElement($hoursOpening)) {
            // set the owning side to null (unless already changed)
            if ($hoursOpening->getDay() === $this) {
                $hoursOpening->setDay(null);
            }
        }

        return $this;
    }

    public function getIdDay(): ?int
    {
        return $this->idDay;
    }

    public function setIdDay(int $idDay): self
    {
        $this->idDay = $idDay;

        return $this;
    }
}
