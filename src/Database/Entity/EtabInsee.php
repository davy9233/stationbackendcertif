<?php

namespace Database\Entity;

use Database\Repository\EtabInseeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EtabInseeRepository::class)]
class EtabInsee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nameUnitLegal = null;

    #[ORM\Column(length: 255)]
    private ?string $siren = null;

    #[ORM\Column(length: 255)]
    private ?string $siret = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $complementAdresseEtablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $numeroVoieEtablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $typeVoieEtablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $libelleVoieEtablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $codePostalEtablissement = null;

    #[ORM\Column(length: 255)]
    private ?string $codeCommuneEtablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $libelleCommuneEtablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $enseigne1Etablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $enseigne2Etablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $enseigne3Etablissement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $denominationUsuelleEtablissement = null;


    #[ORM\Column(length: 255, nullable: true)]
    private ?string $labelAddress = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $houseNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $street = null;


    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nameFormat = null;

    #[ORM\Column(type: 'float', nullable: true)]
    private $latitude;

    #[ORM\Column(type: 'float', nullable: true)]
    private $longitude;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameUnitLegal(): ?string
    {
        return $this->nameUnitLegal;
    }

    public function setNameUnitLegal(?string $nameUnitLegal): static
    {
        $this->nameUnitLegal = $nameUnitLegal;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(string $siren): static
    {
        $this->siren = $siren;

        return $this;
    }


    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): static
    {
        $this->siret = $siret;

        return $this;
    }

    public function getComplementAdresseEtablissement(): ?string
    {
        return $this->complementAdresseEtablissement;
    }

    public function setComplementAdresseEtablissement(?string $complementAdresseEtablissement): static
    {
        $this->complementAdresseEtablissement = $complementAdresseEtablissement;

        return $this;
    }

    public function getNumeroVoieEtablissement(): ?string
    {
        return $this->numeroVoieEtablissement;
    }

    public function setNumeroVoieEtablissement(?string $numeroVoieEtablissement): static
    {
        $this->numeroVoieEtablissement = $numeroVoieEtablissement;

        return $this;
    }

    public function getTypeVoieEtablissement(): ?string
    {
        return $this->typeVoieEtablissement;
    }

    public function setTypeVoieEtablissement(?string $typeVoieEtablissement): static
    {
        $this->typeVoieEtablissement = $typeVoieEtablissement;

        return $this;
    }

    public function getLibelleVoieEtablissement(): ?string
    {
        return $this->libelleVoieEtablissement;
    }

    public function setLibelleVoieEtablissement(?string $libelleVoieEtablissement): static
    {
        $this->libelleVoieEtablissement = $libelleVoieEtablissement;

        return $this;
    }

    public function getCodePostalEtablissement(): ?string
    {
        return $this->codePostalEtablissement;
    }

    public function setCodePostalEtablissement(?string $codePostalEtablissement): static
    {
        $this->codePostalEtablissement = $codePostalEtablissement;

        return $this;
    }

    public function getCodeCommuneEtablissement(): ?string
    {
        return $this->codeCommuneEtablissement;
    }

    public function setCodeCommuneEtablissement(string $codeCommuneEtablissement): static
    {
        $this->codeCommuneEtablissement = $codeCommuneEtablissement;

        return $this;
    }

    public function getLibelleCommuneEtablissement(): ?string
    {
        return $this->libelleCommuneEtablissement;
    }

    public function setLibelleCommuneEtablissement(?string $libelleCommuneEtablissement): static
    {
        $this->libelleCommuneEtablissement = $libelleCommuneEtablissement;

        return $this;
    }

    public function getEnseigne1Etablissement(): ?string
    {
        return $this->enseigne1Etablissement;
    }

    public function setEnseigne1Etablissement(?string $enseigne1Etablissement): static
    {
        $this->enseigne1Etablissement = $enseigne1Etablissement;

        return $this;
    }

    public function getEnseigne2Etablissement(): ?string
    {
        return $this->enseigne2Etablissement;
    }

    public function setEnseigne2Etablissement(?string $enseigne2Etablissement): static
    {
        $this->enseigne2Etablissement = $enseigne2Etablissement;

        return $this;
    }

    public function getEnseigne3Etablissement(): ?string
    {
        return $this->enseigne3Etablissement;
    }

    public function setEnseigne3Etablissement(?string $enseigne3Etablissement): static
    {
        $this->enseigne3Etablissement = $enseigne3Etablissement;

        return $this;
    }

    public function getDenominationUsuelleEtablissement(): ?string
    {
        return $this->denominationUsuelleEtablissement;
    }

    public function setDenominationUsuelleEtablissement(?string $denominationUsuelleEtablissement): static
    {
        $this->denominationUsuelleEtablissement = $denominationUsuelleEtablissement;

        return $this;
    }


    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): static
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getLabelAddress(): ?string
    {
        return $this->labelAddress;
    }

    public function setLabelAddress(string $labelAddress): self
    {
        $this->labelAddress = $labelAddress;

        return $this;
    }


    public function getNameFormat(): ?string
    {
        return $this->nameFormat;
    }

    public function setNameFormat(string $nameFormat): self
    {
        $this->nameFormat = $nameFormat;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

}
