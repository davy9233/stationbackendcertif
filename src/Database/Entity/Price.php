<?php

namespace Database\Entity;

use Doctrine\ORM\Mapping as ORM;
use Database\Repository\PriceRepository;

#[ORM\Entity(repositoryClass: PriceRepository::class)]
class Price
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float', nullable: true)]
    private $value;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $majDate;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $traitementDate;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeCarbu $fuel = null;

    #[ORM\ManyToOne(inversedBy: 'price')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Station $station = null;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?string $valueUpdate = null;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getMajDate(): ?\DateTimeInterface
    {
        return $this->majDate;
    }

    public function setMajDate(\DateTimeInterface $majDate): self
    {
        $this->majDate = $majDate;

        return $this;
    }

    public function getTraitementDate(): ?\DateTimeInterface
    {
        return $this->traitementDate;
    }

    public function setTraitementDate(?\DateTimeInterface $traitementDate): self
    {
        $this->traitementDate = $traitementDate;

        return $this;
    }

    public function getFuel(): ?TypeCarbu
    {
        return $this->fuel;
    }

    public function setFuel(?TypeCarbu $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getValueUpdate(): ?float
    {
        return $this->valueUpdate;
    }

    public function setValueUpdate(float $valueUpdate): self
    {
        $this->valueUpdate = $valueUpdate;

        return $this;
    }

}
