<?php

namespace Database\Entity;

use Doctrine\ORM\Mapping as ORM;
use Database\Repository\StationServiceRepository;

#[ORM\Entity(repositoryClass: StationServiceRepository::class)]
class StationService
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    private ?Station $station = null;

    #[ORM\ManyToOne]
    private ?Service $service = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }
}
