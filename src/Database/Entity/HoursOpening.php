<?php

namespace Database\Entity;

use App\Repository\HoursOpeningRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HoursOpeningRepository::class)]
class HoursOpening
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $open = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $close = null;

    #[ORM\ManyToOne(inversedBy: 'timeTable')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Station $station = null;

    #[ORM\ManyToOne(inversedBy: 'hoursOpenings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Day $day = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOpen(): ?string
    {
        return $this->open;
    }

    public function setOpen(string  $open): self
    {
        $this->open = $open;

        return $this;
    }

    public function getClose(): ?string
    {
        return $this->close;
    }

    public function setClose(string  $close): self
    {
        $this->close = $close;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getDay(): ?Day
    {
        return $this->day;
    }

    public function setDay(?Day $day): self
    {
        $this->day = $day;

        return $this;
    }
}
