<?php

namespace Database\Entity;

use Doctrine\ORM\Mapping as ORM;
use Database\Repository\StationInseeRepository;

#[ORM\Entity(repositoryClass: StationInseeRepository::class)]
class StationInsee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    private ?Station $station = null;

    #[ORM\ManyToOne]
    private ?EtabInsee $etabInsee = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getEtabInsee(): ?EtabInsee
    {
        return $this->etabInsee;
    }

    public function setEtabInsee(?EtabInsee $etabInsee): self
    {
        $this->etabInsee = $etabInsee;

        return $this;
    }
}
