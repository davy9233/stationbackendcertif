<?php

namespace Database\Entity;

use Database\Repository\TypeCarbuRepository;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TypeCarbuRepository::class)]
class TypeCarbu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $shortName;


    #[ORM\Column(type:'integer')]
    private $num = null;


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }


    public function getNum(): ?int
    {
        return $this->num;
    }

    public function setNum(int $num): self
    {
        $this->num = $num;

        return $this;
    }


}
