<?php

namespace Database\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;

use Doctrine\ORM\Mapping as ORM;
use Database\Repository\StationRepository;

#[ORM\Entity(repositoryClass: StationRepository::class)]
class Station
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy:"AUTO")]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $num;

    #[ORM\Column(type: 'float')]
    private $latitude;

    #[ORM\Column(type: 'float')]
    private $longitude;

    #[ORM\Column(type: 'string', length: 255)]
    private $zipCode;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $inseeCode;

    #[ORM\Column(type: 'string', length: 255)]
    private $city;

    #[ORM\Column(type: 'string', length: 255)]
    private $address;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $labelAddress = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $houseNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $street = null;


    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nameFormat = null;

    #[ORM\OneToMany(mappedBy: 'station', targetEntity: Price::class, cascade: ["all"])]
    private Collection $price;

    #[ORM\Column]
    private ?bool $automate = false;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $open = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $closed = null;

    #[ORM\OneToMany(mappedBy: 'station', targetEntity: HoursOpening::class)]
    private Collection $timeTable;

    public function __construct()
    {
        $this->price = new ArrayCollection();
        $this->timeTable = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNum(): ?string
    {
        return $this->num;
    }

    public function setNum(string $num): self
    {
        $this->num = $num;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }


    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getInseeCode(): ?string
    {
        return $this->inseeCode;
    }

    public function setInseeCode(string $inseeCode): self
    {
        $this->inseeCode = $inseeCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Price>
     */
    public function getPrice(): Collection
    {
        return $this->price;
    }

    public function addPrice(Price $price): self
    {
        if (!$this->price->contains($price)) {
            $this->price->add($price);
            $price->setStation($this);
        }

        return $this;
    }

    public function removePrice(Price $price): self
    {
        if ($this->price->removeElement($price)) {
            // set the owning side to null (unless already changed)
            if ($price->getStation() === $this) {
                $price->setStation(null);
            }
        }

        return $this;
    }

    public function isAutomate(): ?bool
    {
        return $this->automate;
    }

    public function setAutomate(bool $automate): self
    {
        $this->automate = $automate;

        return $this;
    }

    public function getOpen(): ?\DateTimeInterface
    {
        return $this->open;
    }

    public function setOpen(?\DateTimeInterface $open): self
    {
        $this->open = $open;

        return $this;
    }

    public function getClosed(): ?\DateTimeInterface
    {
        return $this->closed;
    }

    public function setClosed(?\DateTimeInterface $closed): self
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * @return Collection<int, HoursOpening>
     */
    public function getTimeTable(): Collection
    {
        return $this->timeTable;
    }

    public function addTimeTable(HoursOpening $timeTable): self
    {
        if (!$this->timeTable->contains($timeTable)) {
            $this->timeTable->add($timeTable);
            $timeTable->setStation($this);
        }

        return $this;
    }

    public function removeTimeTable(HoursOpening $timeTable): self
    {
        if ($this->timeTable->removeElement($timeTable)) {
            // set the owning side to null (unless already changed)
            if ($timeTable->getStation() === $this) {
                $timeTable->setStation(null);
            }
        }

        return $this;
    }


    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): static
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getLabelAddress(): ?string
    {
        return $this->labelAddress;
    }

    public function setLabelAddress(string $labelAddress): self
    {
        $this->labelAddress = $labelAddress;

        return $this;
    }


    public function getNameFormat(): ?string
    {
        return $this->nameFormat;
    }

    public function setNameFormat(string $nameFormat): self
    {
        $this->nameFormat = $nameFormat;

        return $this;
    }
}
