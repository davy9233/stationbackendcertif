<?php

namespace Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Database\Entity\Service;

/**
 * @extends ServiceEntityRepository<Service>
 *
 * @method Service|null find($id, $lockMode = null, $lockVersion = null)
 * @method Service|null findOneBy(array $criteria, array $orderBy = null)
 * @method Service[]    findAll()
 * @method Service[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    public function save(Service $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Service $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return [] Returns an array
     */
    public function findAllByField($value)
    {

        $querySelect = 't.' . $value;

        return $this->createQueryBuilder('t')
        ->select($querySelect)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return [] Returns an array
     */
    public function findAllByOneField($value)
    {

        $querySelect = 't.' . $value;

        return $this->createQueryBuilder('t')
        ->select($querySelect)
            ->getQuery()
            ->getSingleColumnResult();
    }


    public function findByArray($ids): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT s FROM App\Entity\Service s
            '
        );
        $result = $query->getResult();
        return $result;
    }

    public function listService(): array
    {

        $rawSql = "SELECT * from service  order by name ";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $result = $stmt->executeQuery()->fetchAllAssociative();

        return $result;
    }

    public function deleteAll()
    {

        $rawSql = "delete from service ";

        $this->getEntityManager()->getConnection()->prepare($rawSql)->executeQuery();

        return true;
    }


    //    /**
    //     * @return Service[] Returns an array of Service objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Service
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
