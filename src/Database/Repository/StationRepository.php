<?php

namespace Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Migrations\Version\State;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\Persistence\ManagerRegistry;
use Domain\Station\DTO\StationDTO;
use Database\Entity\Station;
use Domain\Station\Model\StationCollection;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @extends ServiceEntityRepository<Station>
 *
 * @method Station|null find($id, $lockMode = null, $lockVersion = null)
 * @method Station|null findOneBy(array $criteria, array $orderBy = null)
 * @method Station[]    findAll()
 * @method Station[]|null findAllNumId()
 * @method Station[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Station::class);



    }


    public function add(Station $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Station $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function save(Station $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllNumId(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT id,num FROM station p
            ';
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery();

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }


    public function findLastDate()
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT MAX(maj_date) from price');

        $result = $query->getResult();

        return $result;

    }

    public function findByBbox($bbox)
    {
        /*        $entityManager = $this->getEntityManager();

               $query = $entityManager->createQuery(
                   "SELECT s.id,s.latitude,s.longitude,s.address,s.zipCode,s.city FROM Database\Entity\
Station s
                           WHERE s.latitude < :NElatitude
                   AND s.longitude < :NElongitude
                   AND s.latitude > :SWlatitude
                   AND s.longitude > :SWlongitude
                   "
               )        ->setParameters(array(
                   'NElatitude' => $bbox[0],
                   'NElongitude' => $bbox[1],
                   'SWlatitude' => $bbox[3],
                   'SWlongitude' => $bbox[4],

               ));

               $result = $query->getResult();
               return $result; */
    }


    public function findByBboxByFuel($bbox, $fuel)
    {
        $entityManager = $this->getEntityManager();

        $bbox = explode(",", $bbox);

        $typeMarker = 'marker';

        $limitStation = '1000';

        $stats = [
            'maxPrice' => 'nd' ,
            'minPrice' => 'nd' ,
            'avgPrice' => 'nd',
            'numberResult' => 0
        ];

        if($fuel != 'station' && $fuel) {

            $rawSql = 'SELECT sta.id,sta.latitude,sta.longitude,sta.value_update,sta.short_name,*
            FROM ( SELECT s.id,s.latitude,s.longitude,p.value_update,f.short_name,MIN(p.value_update) as price,p.maj_date  FROM Station as s 
                inner JOIN Price as p
                on s.id = p.station_id
                inner JOIN Type_carbu as f
                on f.id = p.fuel_id
                WHERE s.longitude > :NElongitude
                AND s.longitude < :SWlongitude
                AND s.latitude > :NElatitude
                AND s.latitude < :SWlatitude
                AND f.short_name = :fuel
                GROUP BY s.id,s.latitude,s.longitude,p.value_update,f.short_name,p.maj_date
                ORDER BY p.value_update asc) AS sta LIMIT :limitStation';

            $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
            $stmt->bindValue('NElongitude', floatval($bbox[0]));
            $stmt->bindValue('SWlongitude', floatval($bbox[2]));
            $stmt->bindValue('NElatitude', floatval($bbox[1]));
            $stmt->bindValue('SWlatitude', floatval($bbox[3]));
            $stmt->bindValue('limitStation', $limitStation);
            $stmt->bindValue('fuel', $fuel);

            $result = $stmt->executeQuery()->fetchAllAssociative();

            $max = null;
            $min = null;
            $index = 0;
            $avg = 0;
            $stats = [
                'maxPrice' => 'nd' ,
                'minPrice' => 'nd' ,
                'avgPrice' => 'nd',
                'numberResult' => 0
            ];

            $results = [];

            $position = 0;

            $firstPrice = null ;

            foreach($result as $station) {
                $index++;
                if ($firstPrice == null) {
                    $firstPrice = $station['value_update'];
                    $position++;
                } else {
                    if($firstPrice != $station['value_update']) {
                        $position++;
                    };
                };
                if (key_exists('price', $station)) {
                    $price = $station['price'];

                    $avg = ($avg * ($index - 1) +  floatval($price)) / $index;
                    if ($max == null && $min == null) {
                        $max = $price;
                        $min = $price;
                    } else {
                        if($max < $price) {
                            $max = $price;
                        }
                        if($min > $price) {
                            $min = $price;
                        }

                    }

                }
                $station['price'] = [
                    [
                    'majDate' => $station['maj_date'],
                    'valueUpdate' => $station['value_update'],
                    'fuel' =>  ['shortName' => $station['short_name']],
                    'hit' => $position
                    ]
                ];


                unset($station['value_update']);
                unset($station['short_name']);
                unset($station['maj_date']);
                $results[] = $station;
                $stats = [
                    'maxPrice' => $max ,
                    'minPrice' => $min ,
                    'avgPrice' => $avg,
                    'numberResult' => count($results)
                ];
            }

        } else {

            $query = $entityManager->createQuery(
                'SELECT s FROM Database\Entity\Station s
                WHERE s.longitude > :NElongitude
                AND s.longitude < :SWlongitude
                AND s.latitude > :NElatitude
                AND s.latitude < :SWlatitude'
            )
                ->setMaxResults($limitStation)
                ->setParameters(array(
                    'NElongitude' => floatval($bbox[0]),
                    'SWlongitude' => floatval($bbox[2]),
                    'NElatitude' => floatval($bbox[1]),
                    'SWlatitude' => floatval($bbox[3])

                 ));
            $results = $query->getResult();

            $typeMarker = 'markerCluster';

            $stats['numberResult'] = count($results);

        }

        return [ 'stations' => $results, 'stats' => $stats, 'typeMarker' => $typeMarker];


    }

    public function findByBboxByFuelStats($bbox, $fuel): array
    {
        $entityManager = $this->getEntityManager();

        $bbox = explode(",", $bbox);

        if($fuel != 'station' && $fuel) {

            $query = $entityManager->createQuery(
                'SELECT MAX(p.valueUpdate) as maxPrice,MIN(p.valueUpdate) as minPrice,AVG(p.valueUpdate) as avgPrice, COUNT(s.id) as numberResult FROM Database\Entity\
Station s 
                JOIN Database\Entity\
Price p
                WITH s.id = p.station
                JOIN Database\Entity\
TypeCarbu f
                WITH f.id = p.fuel           
                WHERE s.longitude > :NElongitude
                AND s.longitude < :SWlongitude
                AND s.latitude > :NElatitude
                AND s.latitude < :SWlatitude
                AND f.shortName = :fuel'
            )
            ->setMaxResults(5)
            ->setParameters(array(
                'NElongitude' => floatval($bbox[0]),
                'SWlongitude' => floatval($bbox[2]),
                'NElatitude' => floatval($bbox[1]),
                'SWlatitude' => floatval($bbox[3]),
                 'fuel' => $fuel
             ));
        } else {
            $query = $entityManager->createQuery(
                'SELECT MAX(p.valueUpdate) as maxPrice,MIN(p.valueUpdate) as minPrice,AVG(p.valueUpdate) as avgPrice, COUNT(s.id) as numberResult  FROM Database\Entity\
Station s
                JOIN Database\Entity\
Price p
                WITH s.id = p.station
                JOIN Database\Entity\
TypeCarbu f
                WITH f.id = p.fuel
                WHERE s.longitude > :NElongitude
                AND s.longitude < :SWlongitude
                AND s.latitude > :NElatitude
                AND s.latitude < :SWlatitude'
            )
            ->setMaxResults(5)
            ->setParameters(array(
                'NElongitude' => floatval($bbox[0]),
                'SWlongitude' => floatval($bbox[2]),
                'NElatitude' => floatval($bbox[1]),
                'SWlatitude' => floatval($bbox[3])
             ));

        }
        $result = $query->getSingleResult();

        return $result;
    }

    public function deleteAll()
    {

        $rawSql = "TRUNCATE station RESTART IDENTITY cascade ";

        $this->getEntityManager()->getConnection()->prepare($rawSql)->executeQuery();

        return true;
    }


    //    /**
    //     * @return Station[] Returns an array of Station objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Station
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
