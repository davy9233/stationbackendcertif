<?php

namespace Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Database\Entity\TypeCarbu;

/**
 * @extends ServiceEntityRepository<TypeCarbu>
 *
 * @method TypeCarbu|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCarbu|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCarbu[]    findAll()
 * @method TypeCarbu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCarbuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeCarbu::class);
    }

    public function add(TypeCarbu $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function save(TypeCarbu $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TypeCarbu $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return [] Returns an array
     */
    public function findAllByOneField($value)
    {

        $querySelect = 't.'.$value;

        return $this->createQueryBuilder('t')
        ->select($querySelect)
            ->getQuery()
            ->getSingleColumnResult();
    }

    /**
     * @return [] Returns an array
     */
    public function findAllByField($value)
    {

        $querySelect = 't.'.$value;

        return $this->createQueryBuilder('t')
        ->select($querySelect)
            ->getQuery()
            ->getResult();
    }

    public function getListFuel()
    {

        $rawSql = "SELECT  id,short_name FROM type_carbu";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $typeCarbus = $stmt->executeQuery()->fetchAllAssociative();

        $result = array_merge([["id" => 0,"short_name" => "station"]], $typeCarbus);

        return $result;
    }

    public function deleteAll()
    {

        $rawSql = "TRUNCATE type_carbu RESTART IDENTITY cascade ";

        $this->getEntityManager()->getConnection()->prepare($rawSql)->executeQuery();

        $rawSql = "TRUNCATE type_carbu RESTART IDENTITY cascade";

        $this->getEntityManager()->getConnection()->prepare($rawSql)->executeQuery();

        return true;
    }

    //    /**
    //     * @return TypeCarbu[] Returns an array of TypeCarbu objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('t.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?TypeCarbu
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
