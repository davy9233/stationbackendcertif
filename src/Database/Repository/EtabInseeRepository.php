<?php

namespace Database\Repository;

use Database\Entity\EtabInsee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EtabInsee>
 *
 * @method EtabInsee|null find($id, $lockMode = null, $lockVersion = null)
 * @method EtabInsee|null findOneBy(array $criteria, array $orderBy = null)
 * @method EtabInsee[]    findAll()
 * @method EtabInsee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtabInseeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EtabInsee::class);
    }


    public function save(EtabInsee $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return EtabInsee[] Returns an array of EtabInsee objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('e.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?EtabInsee
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
