<?php

namespace Database\Repository;

use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Database\Entity\Price;

/**
 * @extends ServiceEntityRepository<Price>
 *
 * @method Price|null find($id, $lockMode = null, $lockVersion = null)
 * @method Price|null findOneBy(array $criteria, array $orderBy = null)
 * @method Price[]    findAll()
 * @method Price[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Price[]    findByDateTypeHits()
 */
class PriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Price::class);
    }

    public function add(Price $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Price $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function save(Price $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByDateTypeHits($valueType, $limitResp, $valueDate)
    {
        $entityManager = $this->getEntityManager();


        $query = $entityManager->createQuery(
            'SELECT p
            FROM App\Entity\Price p
            WHERE p.majDate > :majDate
            AND p.typeCarbu = :typeCarbu
            ORDER BY p.value ASC'
        )
        ->setParameter('majDate', new DateTime($valueDate))
        ->setParameter('typeCarbu', $valueType)
        ->setMaxResults($limitResp);

        $result = $query->getResult();

        return $result;

    }

    public function findByDateTypeHitsCP($valueType, $limitResp, $valueDate, $CP)
    {
        $entityManager = $this->getEntityManager();


        $query = $entityManager->createQuery(
            'SELECT p,s
           FROM App\Entity\Price p 
           INNER JOIN p.station s
           WHERE p.majDate > :majDate
           AND p.typeCarbu = :typeCarbu
           AND s.zipCode LIKE :zipCode
           ORDER BY p.value ASC'
        )
        ->setParameters(array(
            'majDate' => new DateTime($valueDate),
            'zipCode' => $CP . '%',
             'typeCarbu' => $valueType))
        ->setMaxResults($limitResp);

        $result = $query->getResult();
        return $result;

    }

    public function getHitPrice($day)
    {

        $dateNow = new DateTime();

        $dateLimit = $dateNow->sub(new DateInterval("P" . $day . "D"))->format('Y-m-d');

        $rawSql = "SELECT  * from price
        inner join (SELECT  fuel_id ,min(value_update)as price from price where maj_date > :majDate  
        group by fuel_id )as b on price=value_update and  price.fuel_id =b.fuel_id 
        inner join station s 	on station_id =s.id
        inner join type_carbu tc  	on  b.fuel_id  =tc.id
        where maj_date > :majDate 
        order by price.fuel_id ,price";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->bindValue('majDate', $dateLimit);
        $result = $stmt->executeQuery()->fetchAllAssociative();

        $NE = ['latitude' => null,'longitude' => null];
        $SW =  ['latitude' => null,'longitude' => null];

        foreach($result as $row) {


            if ($row['latitude'] > $NE['latitude'] || $NE['latitude'] == null) {
                $NE['latitude'] = $row['latitude'];
            };
            if ($row['latitude'] < $SW['latitude'] || $SW['latitude'] == null) {
                $SW['latitude'] = $row['latitude'];
            };
            if ($row['longitude'] > $NE['longitude'] | $NE['longitude'] == null) {
                $NE['longitude'] = $row['longitude'];
            };
            if ($row['longitude'] < $SW['longitude'] | $SW['longitude'] == null) {
                $SW['longitude'] = $row['longitude'];
            };


        }

        $list = [];
        foreach ($result as $row) {

            $price = [
                'id' => $row['station_id'],
                'latitude' => $row['latitude'],
                'longitude' => $row['longitude'],
                'price' => [
                    [
                    'majDate' => $row['maj_date'],
                    'fuel' => [
                        'shortName' => $row['short_name'],
                        'id' => $row['fuel_id']
                    ],

                    'valueUpdate' => $row['price'],
                ]
                ]
                ];
            $list[] = $price;
        }

        return ['stations' => $list, 'border' => ['NE' => $NE,'SW' => $SW]];
        ;

    }

    public function getBadPrice($day)
    {

        $dateNow = new DateTime();

        $dateLimit = $dateNow->sub(new DateInterval("P" . $day . "D"))->format('Y-m-d');

        $rawSql = "SELECT  * from price
        inner join (SELECT  fuel_id ,max(value_update)as price from price where maj_date > :majDate  
        group by fuel_id )as b on price=value_update and  price.fuel_id =b.fuel_id 
        inner join station s 	on station_id =s.id
        inner join type_carbu tc  	on  b.fuel_id  =tc.id
        where maj_date > :majDate 
        order by price.fuel_id ,price";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->bindValue('majDate', $dateLimit);
        $result = $stmt->executeQuery()->fetchAllAssociative();

        $NE = ['latitude' => null,'longitude' => null];
        $SW =  ['latitude' => null,'longitude' => null];

        foreach($result as $row) {


            if ($row['latitude'] > $NE['latitude'] || $NE['latitude'] == null) {
                $NE['latitude'] = $row['latitude'];
            };
            if ($row['latitude'] < $SW['latitude'] || $SW['latitude'] == null) {
                $SW['latitude'] = $row['latitude'];
            };
            if ($row['longitude'] > $NE['longitude'] | $NE['longitude'] == null) {
                $NE['longitude'] = $row['longitude'];
            };
            if ($row['longitude'] < $SW['longitude'] | $SW['longitude'] == null) {
                $SW['longitude'] = $row['longitude'];
            };


        }

        $list = [];
        foreach ($result as $row) {

            $price = [
                'id' => $row['station_id'],
                'latitude' => $row['latitude'],
                'longitude' => $row['longitude'],
                'price' => [
                    [
                    'majDate' => $row['maj_date'],
                    'fuel' => [
                        'shortName' => $row['short_name'],
                        'id' => $row['fuel_id']
                    ],

                    'valueUpdate' => $row['price'],
                ]
                ]
                ];
            $list[] = $price;
        }

        return ['stations' => $list, 'border' => ['NE' => $NE,'SW' => $SW]];


    }

    public function findPriceHit($day = 2, $maxResults = 1)
    {
        $entityManager = $this->getEntityManager();

        $dateNow = new DateTime();

        $dateLimit = $dateNow->sub(new DateInterval("P" . $day . "D"));



        $query = $entityManager->createQuery(
            'SELECT f.id
            FROM App\Entity\TypeCarbu f'
        );

        $resultCarbu = $query->getResult();
        $results = [];

        foreach($resultCarbu as $carbu) {
            $query = $entityManager->createQuery(
                'SELECT f.id,f.shortName as fuel,p.valueUpdate,p.majDate,s.id as stationId
          FROM App\Entity\Price p
          JOIN App\Entity\TypeCarbu as f
          WITH f.id = p.fuel
          JOIN App\Entity\Station as s
          WITH p.station = s.id
          WHERE p.majDate > :majDate
          AND f.id = :fuel
          ORDER BY p.valueUpdate ASC'
            )
            ->setParameters(['majDate' => $dateLimit,'fuel' => $carbu])
              ->setMaxResults($maxResults);

            $result = $query->getResult();
            $results[] = $result;

        }

        return $results;

    }

    public function findDateUpdate()
    {

        $entityManager = $this->getEntityManager();


        $query = $entityManager->createQuery('SELECT Max(p.majDate) as majDate FROM Database\Entity\Price p');

        $result = $query->getResult();

        return $result[0];

    }

    public function deleteAll()
    {
        $rawSql = "TRUNCATE price RESTART IDENTITY cascade ";

        $this->getEntityManager()->getConnection()->prepare($rawSql)->executeQuery();

        return true;
    }
    //    /**
    //     * @return Price[] Returns an array of Price objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Price
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
