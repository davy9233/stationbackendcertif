<?php

namespace Database\Repository;

use Database\Entity\StationInsee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StationInsee>
 *
 * @method StationInsee|null find($id, $lockMode = null, $lockVersion = null)
 * @method StationInsee|null findOneBy(array $criteria, array $orderBy = null)
 * @method StationInsee[]    findAll()
 * @method StationInsee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationInseeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StationInsee::class);
    }

    public function save(StationInsee $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(StationInsee $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function checkInfo()
    {

        $rawSql = "select ei.id as etabid,s.id  as stationid from station s inner join etab_insee ei  on s.label_address = ei.label_address  ";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);

        $results = $stmt->executeQuery()->fetchAllAssociative();

        return $results;
    }

    public function checkI()
    {

        $rawSql = "select ei.id as etabid,s.id  as stationid from station s inner join etab_insee ei  on s.label_address = ei.label_address  ";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);

        $results = $stmt->executeQuery()->fetchAllAssociative();

        return $results;
    }

}
