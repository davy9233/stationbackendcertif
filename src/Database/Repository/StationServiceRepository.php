<?php

namespace Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Database\Entity\StationService;

/**
 * @extends ServiceEntityRepository<StationService>
 *
 * @method StationService|null find($id, $lockMode = null, $lockVersion = null)
 * @method StationService|null findOneBy(array $criteria, array $orderBy = null)
 * @method StationService[]    findAll()
 * @method StationService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StationService::class);
    }

    public function save(StationService $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(StationService $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllByOneField($value)
    {

        $querySelect = 't.' . $value;

        return $this->createQueryBuilder('t')
        ->select($querySelect)
            ->getQuery()
            ->getSingleColumnResult();
    }

    public function stationByService($bbox, $idService): array
    {

        $bbox = explode(",", $bbox);

        $rawSql = "SELECT s.latitude,s.longitude,s.address,s.zip_code,s.city,se.name as service from station_service ss
                    inner join station s on ss.station_id=s.id 
                    inner join service se on ss.service_id=se.id
                    WHERE service_id = :idService
                    AND s.longitude > :NElongitude
                    AND s.longitude < :SWlongitude
                    AND s.latitude > :NElatitude
                    AND s.latitude < :SWlatitude";
        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->bindValue('idService', $idService);
        $stmt->bindValue('NElongitude', floatval($bbox[0]));
        $stmt->bindValue('SWlongitude', floatval($bbox[2]));
        $stmt->bindValue('NElatitude', floatval($bbox[1]));
        $stmt->bindValue('SWlatitude', floatval($bbox[3]));
        $results = $stmt->executeQuery()->fetchAllAssociative();

        $result = [];

        foreach($results as $station) {
            $temp = $station['service'];

            $station['service'] = [$temp];

            $result[] = $station;
        }

        return $result;

    }

}
