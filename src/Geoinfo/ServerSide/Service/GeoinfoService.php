<?php

declare(strict_types=1);

namespace Geoinfo\ServerSide\Service;

use Database\Entity\EtabInsee;
use Database\Entity\StationInsee;
use Database\Repository\EtabInseeRepository;
use Database\Repository\StationInseeRepository;
use Database\Repository\StationRepository;
use Domain\Geoinfo\Contract\GeoinfoOutInterface;
use Geoinfo\ServerSide\Client\SearchAddressClient;
use Geoinfo\ServerSide\Client\SearchCityClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Doctrine\Persistence\ManagerRegistry;
use Geoinfo\ServerSide\Client\GetStationInsee;
use Shared\ServerSide\Client\GetInseeTokenClient;

final class GeoinfoService implements GeoinfoOutInterface
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly SearchAddressClient $searchAddressClient,
        private readonly SearchCityClient $searchCityClient,
        private readonly GetStationInsee $getStationInsee,
        private readonly StationRepository $stationRepository,
        private readonly StationInseeRepository $stationInseeRepository,
        private readonly EtabInseeRepository $etabInseeRepository,
        private readonly GetInseeTokenClient $getInseeTokenClient,
        private ManagerRegistry $doctrine,
    ) {}


    public function getAddress($search, $limit)
    {
        $response = ($this->searchAddressClient)($search, $limit);

        return $response->toArray()['features'];


    }

    public function getCity($search, $limit)
    {
        $response = ($this->searchCityClient)($search, $limit);

        return $response->toArray();


    }


    public function formatAddress()
    {

        $stations = $this->stationRepository->findBy(['labelAddress' => null]);

        //dd($stations[0]);

        $batch = 0;

        foreach($stations as $station) {

            $search = join(" ", [$station->getAddress(), $station->getZipCode(), $station->getCity()]);

            if($search) {
                if(($this->searchAddressClient)($search, '1')->getStatusCode() === 200) {

                    $address = ($this->searchAddressClient)($search, '1')->toArray()['features'];

                    if(count($address) == 0) {
                        continue;
                    };

                    if(key_exists('name', $address[0]['properties'])) {
                        $station->setNameFormat($address[0]['properties']['name']);
                    }

                    if(key_exists('housenumber', $address[0]['properties'])) {
                        $station->setHouseNumber($address[0]['properties']['housenumber']);
                    }

                    if(key_exists('street', $address[0]['properties'])) {
                        $station->setStreet($address[0]['properties']['street']);
                    }
                    if(key_exists('label', $address[0]['properties'])) {
                        $station->setLabelAddress($address[0]['properties']['label']);
                    }
                    if(key_exists('citycode', $address[0]['properties'])) {
                        $station->setInseeCode($address[0]['properties']['citycode']);
                    }

                    $batch++;
                    $this->stationRepository->save($station);
                    if($batch % 100 == 0) {
                        $this->doctrine->getManager()->flush();
                    }
                }

            }


        }

        $this->doctrine->getManager()->flush();

        return null;
    }

    public function getStationInsee()
    {

        $stations = [];
        $token = ($this->getInseeTokenClient)();
        $curseur = '*';
        $curseurSuivant = null;

        if(($this->getStationInsee)($token, $curseur)->getStatusCode() === 200) {

            $response = (($this->getStationInsee)($token, $curseur))->toArray();

            $stations = $response['etablissements'];


            $curseur = $response['header']['curseur'];
            $curseurSuivant = $response['header']['curseurSuivant'];

            while($curseur !== $curseurSuivant) {
                $response = (($this->getStationInsee)($token, $curseurSuivant))->toArray();

                $stations = \array_merge($stations, $response['etablissements']);

                $curseur = $response['header']['curseur'];
                $curseurSuivant = $response['header']['curseurSuivant'];
            }

        }

        return $stations;
    }


    public function insertInseeStation(array $stations)
    {

        foreach($stations as $station) {
            if(
                $this->etabInseeRepository->findOneBy(['siret' => $station["siret"]])
                || $station["adresseEtablissement"]["codeCommuneEtablissement"] == null

            ) {
                continue;
            }
            try {
                $etabInsee = new EtabInsee();
                $etabInsee
                    ->setSiret($station["siret"])
                    ->setSiren($station["siren"])
                    ->setNameUnitLegal($station["uniteLegale"]["denominationUniteLegale"])
                    ->setLibelleCommuneEtablissement($station["adresseEtablissement"]["libelleCommuneEtablissement"])
                    ->setComplementAdresseEtablissement($station["adresseEtablissement"]["complementAdresseEtablissement"])
                    ->setNumeroVoieEtablissement($station["adresseEtablissement"]["numeroVoieEtablissement"])
                    ->setTypeVoieEtablissement($station["adresseEtablissement"]["typeVoieEtablissement"])
                    ->setCodePostalEtablissement($station["adresseEtablissement"]["codePostalEtablissement"])
                    ->setCodeCommuneEtablissement($station["adresseEtablissement"]["codeCommuneEtablissement"])
                    ->setLibelleVoieEtablissement($station["adresseEtablissement"]["libelleVoieEtablissement"])
                    ->setenseigne1Etablissement($station["periodesEtablissement"][0]["enseigne1Etablissement"])
                    ->setenseigne2Etablissement($station["periodesEtablissement"][0]["enseigne2Etablissement"])
                    ->setenseigne3Etablissement($station["periodesEtablissement"][0]["enseigne3Etablissement"])
                    ->setDenominationUsuelleEtablissement($station["periodesEtablissement"][0]["denominationUsuelleEtablissement"]);
                $this->etabInseeRepository->save($etabInsee);
            } catch (\Exception $e) {
                dump($e->getMessage());
            }
        }
        $this->doctrine->getManager()->flush();
    }

    public function formatAddressInsee()
    {

        $etabInsees = $this->etabInseeRepository->findBy(['labelAddress' => null]);
        //$etabInsees = $this->etabInseeRepository->findAll();

        $batch = 0;

        foreach($etabInsees as $etab) {

            $search = join(
                " ",
                [
                $etab->getNumeroVoieEtablissement(),
                $etab->getTypeVoieEtablissement(),
                $etab->getLibelleVoieEtablissement(),
                $etab->getCodePostalEtablissement(),
                $etab->getLibelleCommuneEtablissement()
                ]
            );


            if($search) {
                if(($this->searchAddressClient)($search, '1')->getStatusCode() === 200) {

                    $address = ($this->searchAddressClient)($search, '1')->toArray()['features'];

                    if(count($address) == 0) {
                        continue;
                    };

                    if(key_exists('name', $address[0]['properties'])) {
                        $etab->setNameFormat($address[0]['properties']['name']);
                    }

                    if(key_exists('housenumber', $address[0]['properties'])) {
                        $etab->setHouseNumber($address[0]['properties']['housenumber']);
                    }

                    if(key_exists('street', $address[0]['properties'])) {
                        $etab->setStreet($address[0]['properties']['street']);
                    }
                    if(key_exists('label', $address[0]['properties'])) {
                        $etab->setLabelAddress($address[0]['properties']['label']);
                    }

                    $etab->setLatitude($address[0]['geometry']['coordinates'][1]);


                    $etab->setLongitude($address[0]['geometry']['coordinates'][0]);


                    $batch++;
                    $this->etabInseeRepository->save($etab);
                    if($batch % 100 == 0) {
                        $this->doctrine->getManager()->flush();
                    }
                }

            }


        }

        $this->doctrine->getManager()->flush();

        return null;
    }

    public function updateStationInsee()
    {
        $result = $this->stationInseeRepository->checkInfo();

        foreach($result as $row) {

            if($this->stationInseeRepository->findOneBy(['station' =>  $row['stationid']])) {

                continue;
            }


            $station = $this->stationRepository->findOneBy(['id' =>  $row['stationid']]);

            $insee = $this->etabInseeRepository->findOneBy(['id' =>  $row['etabid']]);
            //dump('ok');
            $stationInsee = new StationInsee();
            $stationInsee
                ->setStation($station)
                ->setEtabInsee($insee);
            ;
            $this->stationInseeRepository->save($stationInsee);
        }

        $this->doctrine->getManager()->flush();
    }
}
