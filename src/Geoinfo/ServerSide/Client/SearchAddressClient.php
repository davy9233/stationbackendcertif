<?php

declare(strict_types=1);

namespace Geoinfo\ServerSide\Client;

use Shared\ServerSide\Client\ClientTrait;

/**
 * @method Array get(string $url, array $options = [], ?string $type = null)
 */
final class SearchAddressClient
{
    use ClientTrait;

    public function __invoke(string $search, string $limit)
    {

        return  $this->get('search', ['query' => [
               'limit' => $limit,
               'q' => $search,
           ]]);

    }

}
