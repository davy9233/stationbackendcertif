<?php

declare(strict_types=1);

namespace Geoinfo\ServerSide\Client;

use Shared\ServerSide\Client\ClientTrait;

/**
 * @method Array get(string $url, array $options = [], ?string $type = null)
 */
final class SearchCityClient
{
    use ClientTrait;

    public function __invoke(string $search, $limit = '1')
    {
        return $this->get('communes', ['query' => [
            'limit' => $limit,
            'nom' => $search,
        ]]);
    }
}
