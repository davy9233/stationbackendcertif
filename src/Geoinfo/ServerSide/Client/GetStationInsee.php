<?php

declare(strict_types=1);

namespace Geoinfo\ServerSide\Client;

use Shared\ServerSide\Client\ClientTrait;
use Shared\ServerSide\Client\GetInseeTokenClient;

/**
 * @method Array get(string $url, array $options = [], ?string $type = null)
 */
final class GetStationInsee
{
    use ClientTrait;




    public function __invoke($token, $curseur)
    {

        $reponse = $this->get('siret', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ],
            'query' => [
              'q' => 'activitePrincipaleUniteLegale:47.30Z  AND etatAdministratifUniteLegale:A' ,
              'champs' => 'siret,siren,denominationUsuelleEtablissement,enseigne1Etablissement,enseigne2Etablissement,enseigne3Etablissement,denominationUniteLegale,complementAdresseEtablissement,numeroVoieEtablissement,indiceRepetitionEtablissement,typeVoieEtablissement,libelleVoieEtablissement,codePostalEtablissement,libelleCommuneEtablissement,codeCommuneEtablissement',
              'nombre' => '1000',
              'curseur' => $curseur
             ]
            ]);

        return  $reponse;
    }
}
