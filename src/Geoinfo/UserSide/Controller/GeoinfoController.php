<?php

declare(strict_types=1);

namespace Geoinfo\UserSide\Controller;

use Domain\Geoinfo\Contract\GeoinfoInInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class GeoinfoController
{
    public function __construct(
        private readonly GeoinfoInInterface $GeoinfoManager
    ) {
    }

    #[Route('/geoinfo/address', name: 'geoinfo_address', methods:'GET')]
    public function getAddress(Request $Request)
    {
        $search = $Request->get('q');
        $limit = $Request->get('limit');

        return new Response(\json_encode($this->GeoinfoManager->getAddress($search, $limit)));

    }

    #[Route('/geoinfo/city', name: 'geoinfo_city', methods:'GET')]
    public function getCity(Request $Request)
    {
        $search = $Request->get('q');
        $limit = $Request->get('limit');

        return new Response(\json_encode($this->GeoinfoManager->getCity($search, $limit)));

    }
}
