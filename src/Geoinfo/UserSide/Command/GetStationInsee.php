<?php

namespace Geoinfo\UserSide\Command;

use Domain\Geoinfo\Contract\GeoinfoInInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:get-station-insee')]
class GetStationInsee extends Command
{
    public function __construct(
        private readonly GeoinfoInInterface $geoinfoManager
    ) {
        parent::__construct(

        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }

        $section1 = $output->section();
        $section1->writeln([
            'get Station from Insee',
        ]);

        //$stations = $this->geoinfoManager->getStationInsee();


        //$this->geoinfoManager->insertInseeStation($stations);

        //$this->geoinfoManager->formatAddressInsee();

        $this->geoinfoManager->updateStationInsee();


        return Command::SUCCESS;
    }
}
