<?php

namespace Station\UserSide\Command;

use Doctrine\Persistence\ManagerRegistry;
use Domain\Station\Contract\StationInInterface;
use Domain\Station\Contract\TerritorialCommunitiesInInterface;
use Station\ServerSide\Client\GetDepartmentsByRegionClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:get-city')]
class GetCodeCity extends Command
{
    public function __construct(
        private readonly StationInInterface $StationManager
    ) {
        parent::__construct(

        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }

        $section1 = $output->section();
        $section1->writeln([
            'download file station',
        ]);


        $this->StationManager->getFileStation();


        $section2 = $output->section();
        $section2->writeln([
            '================',
            'file downloaded ',
        ]);

        return Command::SUCCESS;
    }
}
