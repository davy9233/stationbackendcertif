<?php

namespace Station\UserSide\Command;

use Doctrine\Persistence\ManagerRegistry;
use Domain\Station\Contract\StationInInterface;
use Domain\Station\Contract\TerritorialCommunitiesInInterface;
use Station\ServerSide\Client\GetDepartmentsByRegionClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:init-base')]
class InitBase extends Command
{
    public function __construct(
        private readonly StationInInterface $StationManager
    ) {
        parent::__construct(

        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }

        $section1 = $output->section();
        $section1->writeln([
            'init table day',
        ]);

        $this->StationManager->getFileStation();


        $section1 = $output->section();
        $section1->writeln([
            'init table day',
        ]);


        $this->StationManager->initDay();


        $section2 = $output->section();
        $section2->writeln([
            '================',
            'initialization finish ',
        ]);

        $section3 = $output->section();
        $section3->writeln([
            '================',
            'init typeCarbu',
        ]);

        $this->StationManager->typeCarbu();

        $section4 = $output->section();
        $section4->writeln([
            '================',
            'initialization typeCarbu ',
        ]);

        $section5 = $output->section();
        $section5->writeln([
            '================',
            'init typeService',
        ]);

        $this->StationManager->typeService();

        $section6 = $output->section();
        $section6->writeln([
            '================',
            'initialization typeService ',
        ]);

        $section7 = $output->section();
        $section7->writeln([
            '================',
            'init station',
        ]);

        $this->StationManager->updateStation();

        $section8 = $output->section();
        $section8->writeln([
            '================',
            'update station ',
        ]);

        $section9 = $output->section();
        $section9->writeln([
            '================',
            'init typeService',
        ]);

        $this->StationManager->typeService();

        $section10 = $output->section();
        $section10->writeln([
            '================',
            'initialization typeService ',
        ]);

        $section11 = $output->section();
        $section11->writeln([
            '================',
            'init price',
        ]);

        $this->StationManager->updatePrice();

        $section12 = $output->section();
        $section12->writeln([
            '================',
            'update price ',
        ]);


        return Command::SUCCESS;
    }
}
