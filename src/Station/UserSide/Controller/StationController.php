<?php

declare(strict_types=1);

namespace Station\UserSide\Controller;

use Domain\Station\Contract\StationInInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class StationController
{
    public function __construct(
        private readonly StationInInterface $stationManager,
    ) {}

    #[Route('/station/checkDate', name: 'app_station', methods:'GET')]
    public function getDate()
    {
        $response = $this->stationManager->getDate();

        return new Response(json_encode($response));
    }

    #[Route('/station/typefuel', name: 'app_station_typefuel', methods: 'GET')]
    public function getListFuel()
    {
        $response = $this->stationManager->getListFuel();

        return new Response(json_encode($response));
    }

    #[Route('/station/hitList', name: 'app_station_hitList', methods:'GET')]
    public function gethitprice(Request $Request)
    {
        $day = $Request->get('day');

        if(!$day) {
            $day = 2 ;
        }

        $response = $this->stationManager->getHitPrice($day);

        return new Response(json_encode($response));
    }

    #[Route('/station/badList', name: 'app_station_badList', methods:'GET')]
    public function getbadprice(Request $Request)
    {
        $day = $Request->get('day');

        if(!$day) {
            $day = 2 ;
        }

        $response = $this->stationManager->getBadPrice($day);

        return new Response(json_encode($response));
    }

    #[Route('/station/bboxByFuel', name: 'app_station_bboxByFuel', methods: 'GET')]
    public function findByBboxByFuel(Request $Request)
    {

        $bbox = $Request->get('bbox');
        $fuel = $Request->get('fuel');

        $response = $this->stationManager->getStationBboxFuel($bbox, $fuel);

        return new Response(json_encode($response));
    }

    #[Route('/station/listService', name: 'app_station_listService', methods:'GET')]
    public function listService()
    {

        $response = $this->stationManager->listService();

        return new Response(json_encode($response));
    }


    #[Route('/station/stationService', name: 'app_station_stationService', methods:'GET')]
    public function stationByService(Request $Request)
    {

        $bbox = $Request->get('bbox');
        $idService = $Request->get('idService');

        $response = $this->stationManager->stationByService($bbox, $idService);

        return new Response(json_encode($response));
    }

}
