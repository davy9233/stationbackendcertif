<?php

declare(strict_types=1);

namespace Station\ServerSide\Service;

use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Domain\Station\Contract\StationOutInterface;
use Domain\Station\Model\DateInfo;
use Exception;
use PHPUnit\Util\Json;
use Database\Entity\Day;
use Database\Entity\HoursOpening;
use Database\Entity\Price;
use Database\Entity\Service;
use Database\Entity\Station;
use Database\Entity\StationService as EntityStationService;
use Database\Entity\TypeCarbu;
use Database\Repository\DayRepository;
use Database\Repository\HoursOpeningRepository;
use Database\Repository\PriceRepository;
use Database\Repository\ServiceRepository;
use Database\Repository\StationRepository;
use Database\Repository\StationServiceRepository;
use Database\Repository\TypeCarbuRepository;
use Error;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use ZipArchive;

final class StationService implements StationOutInterface
{
    public function __construct(
        private readonly PriceRepository $priceRepository,
        private readonly DayRepository $dayRepository,
        private readonly StationRepository $stationRepository,
        private readonly HttpClientInterface $client,
        private readonly TypeCarbuRepository $typeCarbuRepository,
        private readonly HoursOpeningRepository $hoursOpeningRepository,
        private readonly ServiceRepository $serviceRepository,
        private readonly ContainerBagInterface $params,
        private readonly StationServiceRepository $stationServiceRepository,
        private ManagerRegistry $doctrine,
    ) {}

    public function getDate()
    {
        $reponse = $this->priceRepository->findDateUpdate();

        return $reponse;

    }

    public function getFileStation(): bool
    {


        $filesystem = new Filesystem();

        $dir_public = $this->params->get('public_dir');

        $destination = $dir_public . 'uploads/files/station';

        $filesystem->remove($destination);


        $url = "https://donnees.roulez-eco.fr/opendata/instantane";
        $response = $this->client->request('GET', $url);

        if ($response->getStatusCode() == 200) {
            $file = $response->getContent();

            $filename = ($response->getHeaders())["content-disposition"][0];
            $pattern = "/\".*/";
            preg_match($pattern, $filename, $tabFilename);
            $filename = preg_replace('/"/', '', $tabFilename[0]); // Outputs 1

            $filesystem = new Filesystem();

            $fullpath = $destination . $filename;

            $filesystem->dumpFile($fullpath, $file);

            $zip = new ZipArchive();

            if (true === $zip->open($fullpath)) {
                $zip->extractTo($destination);
                $filesystem->remove($fullpath);
                return true;


            } return false;

        }

        return false;

    }

    public function initDay(): bool
    {

        $xmlfile = file_get_contents($this->params->get('public_dir') . '/uploads/files/station/PriceCarburants_instantane.xml');

        $encoder = new XmlEncoder();

        $listpdv = $encoder->decode($xmlfile, 'xml');

        $stations = $listpdv['pdv'];

        $totalDay = 0;
        foreach($stations as $station) {

            try {
                if(key_exists('horaires', $station)) {
                    foreach($station['horaires']['jour'] as $day) {
                        $one = $this->dayRepository->findOneBy(["name" => $day['@nom']]);
                        if($one == null) {
                            $oneDay = new Day();
                            $oneDay
                             ->setIdDay($day['@id'])
                             ->setName($day['@nom']);
                            $this->dayRepository->save($oneDay, true);
                            $totalDay++;
                            if ($totalDay == 7) {
                                return true;
                            }
                        }
                    }
                };
            } catch(Exception $e) {
                continue;
            }
        }

        return true;
    }

    public function typeCarbu()
    {

        $xmlfile = file_get_contents($this->params->get('public_dir') . '/uploads/files/station/PrixCarburants_instantane.xml');

        $encoder = new XmlEncoder();

        $listpdv = $encoder->decode($xmlfile, 'xml');

        $stations = $listpdv['pdv'];

        $fuels = $this->typeCarbuRepository->findAllByOneField('num');

        foreach($stations as $station) {

            if (key_exists('prix', $station)) {
                foreach($station['prix'] as $price) {
                    if(is_array($price)) {
                        if(!in_array($price['@id'], $fuels)) {
                            $fuel = new TypeCarbu();
                            $fuel
                                ->setNum($price['@id'])
                                ->setShortName($price['@nom']);
                            $this->typeCarbuRepository->save($fuel, true);
                            $fuels[] = $price['@id'];
                        }

                    }

                }

            }

        }
    }

    public function updatePrice()
    {

        $xmlfile = file_get_contents($this->params->get('public_dir') . '/uploads/files/station/PrixCarburants_instantane.xml');

        $encoder = new XmlEncoder();

        $listpdv = $encoder->decode($xmlfile, 'xml');

        $stations = $listpdv['pdv'];

        $batch = 0;

        foreach($stations as $station) {

            $batch++;

            $stationOne = $this->stationRepository->findOneBy(['num' => $station['@id']]);

            try {
                if (key_exists('prix', $station)) {
                    foreach($station['prix'] as $price) {
                        if(is_array($price)) {
                            $typeCarbu = $this->typeCarbuRepository->findOneBy(['num' => $price['@id']]);
                            $fuel = $this->priceRepository->findOneBy(['station' => $stationOne,'fuel' => $typeCarbu ]) ?: new Prix();
                            $fuel
                                ->setValueUpdate($price['@valeur'])
                                ->setMajDate(new DateTime($price['@maj']))
                                ->setStation($stationOne)
                                ->setFuel($typeCarbu);
                            $this->priceRepository->save($fuel);
                        };
                    }
                }
            } catch(Exception $e) {
                echo $e->getMessage();
            };


            if(key_exists('horaires', $station)) {
                foreach($station['horaires']['jour'] as $day) {
                    if(key_exists('horaire', $day)) {
                        $horaire = $day['horaire'];
                        if(key_exists('@ouverture', $horaire) && key_exists('@fermeture', $horaire)) {

                            $horaire['@ouverture'] = explode(".", strVal($horaire['@ouverture']));
                            if(count($horaire['@ouverture']) == 1) {
                                $horaire['@ouverture'][] = '00';
                            } else {
                                if(strlen($horaire['@ouverture'][1]) == 1) {
                                    $horaire['@ouverture'][1] = $horaire['@ouverture'][1] . '0';
                                };
                            }

                            $horaire['@ouverture'] = \implode(":", $horaire['@ouverture']);

                            $horaire['@fermeture'] = explode(".", strVal($horaire['@fermeture']));
                            if(count($horaire['@fermeture']) == 1) {
                                $horaire['@fermeture'][] = '00';
                            } else {
                                if(strlen($horaire['@fermeture'][1]) == 1) {
                                    $horaire['@fermeture'][1] = $horaire['@fermeture'][1] . '0';
                                };
                            }

                            $horaire['@fermeture'] = \implode(":", $horaire['@fermeture']);

                            $dayOne = $this->dayRepository->findOneBy(['idDay' => $day['@id']]);
                            $hourDay = $this->hoursOpeningRepository->findOneBy(['station' => $stationOne ,'day' => $dayOne]) ?: new HoursOpening();
                            $hourDay
                                ->setOpen($horaire['@ouverture'])
                                ->setClose($horaire['@fermeture'])
                                ->setStation($stationOne)
                                ->setDay($dayOne);
                            $this->hoursOpeningRepository->save($hourDay);

                        }
                    }
                }
            };
            if($batch % 1000 == 0) {
                $this->doctrine->getManager()->flush();
                $batch = 0;
            }
        };
        $this->doctrine->getManager()->flush();

    }

    public function updateStation()
    {
        $xmlfile = file_get_contents($this->params->get('public_dir') . '/uploads/files/station/PrixCarburants_instantane.xml');

        $encoder = new XmlEncoder();

        $listpdv = $encoder->decode($xmlfile, 'xml');

        $stations = $listpdv['pdv'];



        foreach($stations as $station) {


            $stationOne = $this->stationRepository->findOneBy(['num' => $station['@id']]) ?: new Station();

            $automate = false;

            if (key_exists('horaires', $station)) {
                if ($station['horaires']['@automate-24-24'] == '1') {
                    $automate = true;
                };
            }

            $stationOne
                ->setNum(strval($station['@id']))
                ->setLatitude($station['@latitude'] / 100000)
                ->setLongitude($station['@longitude'] / 100000)
                ->setAddress(mb_strtoupper($station['adresse']))
                ->setCity(mb_strtoupper($station['ville']))
                ->setAutomate($automate)
                ->setZipCode(strval($station['@cp']));
            $this->stationRepository->save($stationOne);
        }

        $this->doctrine->getManager()->flush();

    }

    public function typeService()
    {
        $xmlfile = file_get_contents($this->params->get('public_dir') . '/uploads/files/station/PrixCarburants_instantane.xml');

        $encoder = new XmlEncoder();

        $listpdv = $encoder->decode($xmlfile, 'xml');

        $stations = $listpdv['pdv'];

        $this->serviceRepository->deleteAll();

        foreach($stations as $station) {

            if(key_exists('services', $station)) {
                if(is_array($station['services'])) {
                    if(key_exists('service', $station['services'])) {
                        if(is_array($station['services']['service'])) {
                            foreach($station['services']['service'] as $service) {
                                $serviceOne = $this->serviceRepository->findOneBy(['name' => $service]) ?? new Service();
                                if($serviceOne->getId() == null) {
                                    $serviceOne->setName($service);
                                    $this->serviceRepository->save($serviceOne, true);

                                };
                            }
                        }
                    }
                }
            }
        }
    }


    public function updateService()
    {

        $xmlfile = file_get_contents($this->params->get('public_dir') . '/uploads/files/station/PrixCarburants_instantane.xml');

        $encoder = new XmlEncoder();

        $listpdv = $encoder->decode($xmlfile, 'xml');

        $stations = $listpdv['pdv'];
        $days = [];
        $services = [];
        $horaires = [];
        $prices = [];
        $typeCarbu = [];
        $automates = [];

        foreach($stations as  $station) {
            try {
                if (\key_exists('services', $station)) {
                    if(is_array($station['services'])) {
                        if (\key_exists('service', $station['services'])) {
                            if(\is_string($station['services']['service'])) {
                                $station['services']['service'] = [$station['services']['service']];
                            };
                            $services[$station['@id']] = $station['services']['service'];

                        }
                    }
                }
            } catch(Error $e) {
                continue;
            }
            try {
                if (\key_exists('horaires', $station)) {
                    $horaires[$station['@id']] = $station['horaires'];
                }
            } catch(Error $e) {
                continue;
            }
            try {
                if (\key_exists('prix', $station)) {
                    if (\key_exists('@id', $station['prix'])) {
                        $station['prix'] = [$station['prix']];
                    }
                    $prices[$station['@id']] = $station['prix'];
                    if(is_array($station['prix'])) {
                        foreach($station['prix'] as $price) {
                            $typeCarbu[$price['@id']] = $price['@nom'];
                        }
                    }
                }
            } catch(Error $e) {
                continue;
            }
            try {
                if(key_exists('horaires', $station)) {
                    if(key_exists('@automate-24-24', $station)) {
                        $automates[$station['@id']] = $station['@automate-24-24'];
                    }
                    $horaires[$station['@id']] = $station['horaires']['jour'];
                    foreach($station['horaires']['jour'] as $day) {
                        $days[$day['@id']] = $day['@nom'];
                        if(key_exists('horaire', $day)) {
                            $horaire = $day['horaire'];
                            if(key_exists('@ouverture', $horaire) && key_exists('@fermeture', $horaire)) {

                                $horaire['@ouverture'] = explode(".", strVal($horaire['@ouverture']));
                                if(count($horaire['@ouverture']) == 1) {
                                    $horaire['@ouverture'][] = '00';
                                } else {
                                    if(strlen($horaire['@ouverture'][1]) == 1) {
                                        $horaire['@ouverture'][1] = $horaire['@ouverture'][1] . '0';
                                    };
                                }

                                $horaire['@ouverture'] = \implode(":", $horaire['@ouverture']);

                                $horaire['@fermeture'] = explode(".", strVal($horaire['@fermeture']));
                                if(count($horaire['@fermeture']) == 1) {
                                    $horaire['@fermeture'][] = '00';
                                } else {
                                    if(strlen($horaire['@fermeture'][1]) == 1) {
                                        $horaire['@fermeture'][1] = $horaire['@fermeture'][1] . '0';
                                    };
                                }

                                $horaire['@fermeture'] = \implode(":", $horaire['@fermeture']);

                            }

                        }


                    };
                }
            } catch(Error $e) {
                continue;
            }

        };

        $this->typeCarbuRepository->deleteAll();
        $this->stationRepository->deleteAll();
        $this->priceRepository->deleteAll();
        $this->dayRepository->deleteAll();

        foreach($typeCarbu as $key => $value) {
            $fuel = new TypeCarbu();
            $fuel
                ->setNum($key)
                ->setShortName($value);
            $this->typeCarbuRepository->save($fuel);
        }

        foreach($days as $key => $value) {
            $day = new Day();
            $day
                ->setIdDay($key)
                ->setName($value);
            $this->dayRepository->save($day);
        }

        foreach($stations as $station) {
            $stationOne = new Station();
            $stationOne
                ->setNum(strval($station['@id']))
                ->setLatitude($station['@latitude'] / 100000)
                ->setLongitude($station['@longitude'] / 100000)
                ->setAddress(mb_strtoupper($station['adresse']))
                ->setCity(mb_strtoupper($station['ville']))
                ->setZipCode(strval($station['@cp']));
            $this->stationRepository->save($stationOne);
        }

        $this->doctrine->getManager()->flush();

        foreach($prices as $key => $pricesFuel) {
            foreach($pricesFuel as $priceFuel) {
                $price = new Price();
                $typeCarbu = $this->typeCarbuRepository->findOneBy(['num' => $priceFuel['@id']]);
                $station = $this->stationRepository->findOneBy(['num' => $key]);
                $price
                    ->setValueUpdate($priceFuel['@valeur'])
                    ->setMajDate(new DateTime($priceFuel['@maj']))
                    ->setStation($station)
                    ->setFuel($typeCarbu);
                $this->priceRepository->save($price);
            }
        }

        //$this->doctrine->getManager()->flush();

        foreach($services as $key => $servicesInfo) {

            foreach($servicesInfo as $serviceInfo) {
                $serviceStation = new EntityStationService();
                $service = $this->serviceRepository->findOneBy(['name' => $serviceInfo]);
                $station = $this->stationRepository->findOneBy(['num' => $key]);
                $serviceStation
                    ->setStation($station)
                    ->setService($service);
                $this->stationServiceRepository->save($serviceStation);

            }

        }

        $this->doctrine->getManager()->flush();

    }

    public function getHitPrice($day)
    {
        return $this->priceRepository->getHitPrice($day);

    }

    public function getBadPrice($day)
    {
        return  $this->priceRepository->getBadPrice($day);

    }

    public function getStationBboxFuel($bbox, $fuel)
    {
        return  $this->stationRepository->findByBboxByFuel($bbox, $fuel);
    }

    public function getListFuel()
    {
        return $this->typeCarbuRepository->getListFuel();
    }


    public function findByBboxByFuelStats($bbox, $fuel)
    {
        return  $this->stationRepository->findByBboxByFuelStats($bbox, $fuel);
    }

    public function listFuel()
    {
        return $this->typeCarbuRepository->getListFuel();
    }

    public function listService()
    {
        return $this->serviceRepository->listService();
    }

    public function stationByService($bbox, $idService)
    {
        return $this->stationServiceRepository->stationByService($bbox, $idService);

    }

}
