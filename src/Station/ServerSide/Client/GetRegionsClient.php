<?php

declare(strict_types=1);

namespace Station\ServerSide\Client;

use Domain\Station\DTO\RegionDTO;
use Domain\Station\Model\Region;
use Domain\Station\Model\RegionCollection;
use Shared\ServerSide\Client\ClientTrait;
use Database\Entity\Region as EntityRegion;

/**
 * @method RegionCollection get(string $url, array $options = [], ?string $type = null)
 */
final class GetRegionsClient
{
    use ClientTrait;

    public function __invoke()
    {
        $response = $this->get('https://geo.api.gouv.fr/regions', [], RegionCollection::class);

        return RegionCollection();
    }

}
