<?php

declare(strict_types=1);

namespace Station\ServerSide\Restructurator;

use Station\ServerSide\Restructurator\GetRegionRestructurator;
use Shared\ServerSide\Restructurator\RestructuratorInterface;

final class GetRegionsRestructurator implements RestructuratorInterface
{
    public function restructure(array $data): array
    {
        $regionRestructurator = new GetRegionRestructurator();

        return [
            'regions' => array_map(
                fn (array $department): array => $regionRestructurator->restructure($department),
                $data
            )
        ];
    }
}
