<?php

declare(strict_types=1);

namespace Station\ServerSide\Restructurator;

use Shared\ServerSide\Restructurator\RestructuratorInterface;

final class GetRegionRestructurator implements RestructuratorInterface
{
    public function restructure(array $data): array
    {
        return [
            'name' => $data['nom'],
            'code' => $data['code'],
        ];
    }
}
