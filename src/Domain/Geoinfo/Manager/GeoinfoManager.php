<?php

declare(strict_types=1);

namespace Domain\Geoinfo\Manager;

use Domain\Geoinfo\Contract\GeoinfoInInterface;
use Domain\Geoinfo\Contract\GeoinfoOutInterface;

final class GeoinfoManager implements GeoinfoInInterface
{
    public function __construct(private readonly GeoinfoOutInterface $service) {}

    public function getAddress($search, $limit)
    {

        $response = $this->service->getAddress($search, $limit);

        return $response;
    }

    public function getCity($search, $limit)
    {

        return $this->service->getCity($search, $limit);

    }

    public function formatAddress()
    {
        return $this->service->formatAddress();

    }

    public function getStationInsee()
    {

        return $this->service->getStationInsee();
    }

    public function insertInseeStation(array $etab)
    {
        return $this->service->insertInseeStation($etab);
    }

    public function formatAddressInsee()
    {
        return $this->service->formatAddressInsee();

    }

    public function updateStationInsee()
    {
        $response = $this->service->updateStationInsee();
        dd($response);
        return $response;
    }
}
