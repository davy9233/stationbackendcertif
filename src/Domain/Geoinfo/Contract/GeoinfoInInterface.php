<?php

declare(strict_types=1);

namespace Domain\Geoinfo\Contract;

interface GeoinfoInInterface
{
    public function getAddress($search, $limit);
    public function getCity($search, $limit);
    public function formatAddress();
    public function getStationInsee();
    public function insertInseeStation(array $etab);
    public function formatAddressInsee();
    public function updateStationInsee();
}
