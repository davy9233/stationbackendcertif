<?php

declare(strict_types=1);

namespace Domain\Station\Contract;

interface StationInInterface
{
    public function getDate();
    public function getFileStation();
    public function initDay();
    public function typeCarbu();
    public function typeService();
    public function updatePrice();
    public function updateStation();
    public function getHitprice($day);
    public function getBadprice($day);
    public function getStationBboxFuel($bbox, $fuel);
    public function getListFuel();
    public function listService();
    public function stationByService($bbox, $idService);
    public function updateService();
}
