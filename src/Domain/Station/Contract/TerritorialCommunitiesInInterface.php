<?php

declare(strict_types=1);

namespace Domain\Station\Contract;

use Domain\Station\Model\RegionCollection;

interface TerritorialCommunitiesInInterface
{
    public function getRegions(): RegionCollection;
}
