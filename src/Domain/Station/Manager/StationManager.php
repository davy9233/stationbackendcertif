<?php

declare(strict_types=1);

namespace Domain\Station\Manager;

use Domain\Station\Contract\StationInInterface;
use Domain\Station\Contract\StationOutInterface;
use Domain\Station\DTO\BboxByFuelDTO;
use Domain\Station\Model\Station;
use Domain\Station\Model\StationCollection;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

final class StationManager implements StationInInterface
{
    public function __construct(private readonly StationOutInterface $service) {}

    public function getStationByFuel(BboxByFuelDTO $dto, string $fuel): StationCollection
    {

        return new StationCollection();
    }

    public function getHitStationAllFuel(): StationCollection
    {
        return new StationCollection();
    }

    public function get(string $StationId): Station
    {
        return new Station();
    }

    public function getDate()
    {
        return $this->service->getDate();
    }

    public function getFileStation()
    {
        $this->service->getFileStation();
    }

    public function initDay()
    {
        $this->service->initDay();
    }

    public function typeCarbu()
    {
        $this->service->typeCarbu();
    }

    public function updatePrice()
    {
        $this->service->updatePrice();
    }

    public function updateStation()
    {
        $this->service->updateStation();
    }

    public function typeService()
    {
        $this->service->typeService();
    }

    public function getHitPrice($day)
    {
        $response = $this->service->getHitPrice($day);

        return $response;
    }

    public function getBadPrice($day)
    {
        $response = $this->service->getBadPrice($day);

        return $response;
    }

    public function getStationBboxFuel($bbox, $fuel)
    {
        $info = $this->service->getStationBboxFuel($bbox, $fuel);

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $stationJson = ($serializer->serialize($info['stations'], 'json', [AbstractNormalizer::ATTRIBUTES => ['id' => 'id','longitude' => 'longitude','latitude' => 'latitude','price' => ['valueUpdate' => 'valueUpdate','fuel' => ['shortName' => 'shortname']]]]));

        $apiStation = ['stats' => $info['stats'], 'stations' => json_decode($stationJson), 'typeMarker' => $info['typeMarker']];

        return ($apiStation);

    }

    public function getListFuel()
    {
        return $this->service->getListFuel();
    }

    public function listService()
    {
        return $this->service->listService();
    }

    public function stationByService($bbox, $idService)
    {
        $stations = $this->service->stationByService($bbox, $idService);
        $stationsStat = [
            'minPrice' => 'ND',
            'maxPrice' => 'ND',
            'avgPrice' => 'ND',
            'numberResult' => count($stations),
        ];
        return  ['stats' => $stationsStat,'stations' => $stations];
    }

    public function updateService()
    {
        return $this->service->updateService();
    }
}
