<?php

declare(strict_types=1);

namespace Domain\Station\Model;

use Domain\Station\Model\Region ;

abstract class RegionCollection
{
    /** @var Region[] */
    private array $regions;

    public function __construct(array $regions = [])
    {
        $this->setRegions($regions);
    }

    public function setRegions(array $regions): self
    {
        foreach ($regions as $region) {
            $this->addRegions($region);
        }

        return $this;
    }

    /** @return Regions[] */
    public function getRegions(): array
    {
        return $this->regions;
    }

    private function addRegions(Region $Region): void
    {
        $this->regions[] = $Region;
    }
}
