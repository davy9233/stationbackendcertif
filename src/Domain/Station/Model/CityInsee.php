<?php

declare(strict_types=1);

namespace Domain\Station\Model;

abstract class CityInsee
{
    private $name;

    private $insee;

    private $postal;

    private $codeDepartment;

    private $codeRegion;



    final public function getName(): string
    {
        return $this->name;
    }

    final public function getInsee(): string
    {
        return $this->insee;
    }

    final public function getPostal(): string
    {
        return $this->postal;
    }

    final public function getcodeDepartment(): string
    {
        return $this->codeDepartment;
    }


    final public function getCodeRegion(): string
    {
        return $this->codeRegion;
    }

}
