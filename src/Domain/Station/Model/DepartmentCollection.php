<?php

declare(strict_types=1);

namespace Domain\Station\Model;

use Domain\Station\Model\Department ;

abstract class DepartmentCollection
{
    /** @var Department[] */
    private array $Departments;

    public function __construct(array $Departments = [])
    {
        $this->setDepartment($Departments);
    }

    public function setDepartment(array $Departments): self
    {
        foreach ($Departments as $Department) {
            $this->addDepartment($Department);
        }

        return $this;
    }

    /** @return Departments[] */
    public function getDepartment(): array
    {
        return $this->Departments;
    }

    private function addDepartment(Department $Department): void
    {
        $this->Departments[] = $Department;
    }
}
