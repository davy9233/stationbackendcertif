<?php

declare(strict_types=1);

namespace Domain\Station\Model;

use Domain\Station\Model\CityInsee ;

abstract class CityInseeCollection
{
    /** @var CityInsee[] */
    private array $CityInsees;

    public function __construct(array $CityInsees = [])
    {
        $this->setCityInsee($CityInsees);
    }

    public function setCityInsee(array $CityInsees): self
    {
        foreach ($CityInsees as $CityInsee) {
            $this->addCityInsee($CityInsee);
        }

        return $this;
    }

    /** @return CityInsees[] */
    public function getCityInsee(): array
    {
        return $this->CityInsees;
    }

    private function addCityInsee(CityInsee $CityInsee): void
    {
        $this->CityInsees[] = $CityInsee;
    }
}
