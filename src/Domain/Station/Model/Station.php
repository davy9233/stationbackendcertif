<?php

declare(strict_types=1);

namespace Domain\Station\Model;

abstract class Station
{
    private $num;

    private $latitude;

    private $longitude;

    private $zipCode;

    private $city;

    private $address;

    final public function getNum(): string
    {
        return $this->num;
    }


    final public function getLatitude(): string
    {
        return $this->latitude;
    }

    final public function getLongitude(): string
    {
        return $this->longitude;
    }

    final public function getZipCode(): string
    {
        return $this->zipCode;
    }

    final public function getCity(): string
    {
        return $this->city;
    }

    final public function getAddress(): string
    {
        return $this->address;
    }
}
