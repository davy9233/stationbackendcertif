<?php

declare(strict_types=1);

namespace Domain\Station\Model;

abstract class Department
{
    private $name;

    private $code;


    final public function getName(): string
    {
        return $this->name;
    }


    final public function getCode(): string
    {
        return $this->code;
    }
}
