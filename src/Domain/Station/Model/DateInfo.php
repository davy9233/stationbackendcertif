<?php

declare(strict_types=1);

namespace Domain\Station\Model;

abstract class DateInfo
{
    private $year;

    private $month;

    private $day;

    final public function getYear(): string
    {
        return $this->year;
    }

    final public function getMonth(): string
    {
        return $this->month;
    }

    final public function getDay(): string
    {
        return $this->day;
    }
}
