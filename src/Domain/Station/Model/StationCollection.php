<?php

declare(strict_types=1);

namespace Domain\Station\Model;

use Domain\Station\Model\Station ;

abstract class StationCollection
{
    /** @var Station[] */
    private array $stations;

    public function __construct(array $stations = [])
    {
        $this->setStations($stations);
    }

    public function setStations(array $stations): self
    {
        foreach ($stations as $station) {
            $this->addStations($station);
        }

        return $this;
    }

    /** @return Stations[] */
    public function getStations(): array
    {
        return $this->stations;
    }

    private function addStations(Station $Station): void
    {
        $this->stations[] = $Station;
    }
}
