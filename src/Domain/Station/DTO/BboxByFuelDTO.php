<?php

declare(strict_types=1);

namespace Domain\Station\DTO;

use Assert\Assert;
use Assert\LazyAssertion;
use Shared\Domain\DTO\ValidatorTrait;

final class BboxByFuelDTO
{
    use ValidatorTrait;

    public function __construct(
        private readonly array $bbox,
        private readonly string $fuel
    ) {
    }

    public function getBbox(): array
    {
        return $this->bbox;
    }

    public function getFuel(): string
    {
        return $this->fuel;
    }

    private function assertRules(): LazyAssertion
    {
        $lazyAssert = Assert::lazy()->tryAll()
            ->that($this->bbox, 'bbox')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string');

        return $lazyAssert;
    }

}
