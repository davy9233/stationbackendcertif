<?php

declare(strict_types=1);

namespace Domain\Station\DTO;

use Assert\Assert;
use Assert\LazyAssertion;
use Shared\Domain\DTO\ValidatorTrait;

final class RegionDTO
{
    use ValidatorTrait;

    public function __construct(
        private readonly string $name,
        private readonly string $code,
    ) {
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    private function assertRules(): LazyAssertion
    {
        $lazyAssert = Assert::lazy()->tryAll()
            ->that($this->name, 'num')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->code, 'zipCode')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string');
        ;

        return $lazyAssert;
    }

}
