<?php

declare(strict_types=1);

namespace Domain\Station\DTO;

use Assert\Assert;
use Assert\LazyAssertion;
use Shared\Domain\DTO\ValidatorTrait;

final class RegionsDTO
{
    use ValidatorTrait;


    public function __construct(private readonly string $regionCode)
    {
    }

    public function getRegionCode(): string
    {
        return $this->regionCode;
    }

    private function assertRules(): LazyAssertion
    {
        $lazyAssert = Assert::lazy()->tryAll()
            ->that($this->regionCode, 'regionCode')
            ->notBlank('errors.region_code_must_be_defined')
            ->string('errors.region_code_must_be_string')
            ->maxLength(2, 'errors.region_code_must_be_valid');

        return $lazyAssert;
    }
}
