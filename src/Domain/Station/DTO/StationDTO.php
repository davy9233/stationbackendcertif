<?php

declare(strict_types=1);

namespace Domain\Station\DTO;

use Assert\Assert;
use Assert\LazyAssertion;
use Shared\Domain\DTO\ValidatorTrait;

final class StationDTO
{
    use ValidatorTrait;

    public function __construct(
        private readonly string $num,
        private readonly string $latitude,
        private readonly string $longitude,
        private readonly string $zipCode,
        private readonly string $inseeCode,
        private readonly string $address,
    ) {
    }


    public function getNum(): ?string
    {
        return $this->num;
    }


    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function getInseeCode(): ?string
    {
        return $this->inseeCode;
    }


    public function getAddress(): ?string
    {
        return $this->address;
    }


    private function assertRules(): LazyAssertion
    {
        $lazyAssert = Assert::lazy()->tryAll()
            ->that($this->num, 'num')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->latitude, 'latitude')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->longitude, 'longitude')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->zipCode, 'zipCode')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->inseeCode, 'inseeCode')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string');
        ;

        return $lazyAssert;
    }

}
