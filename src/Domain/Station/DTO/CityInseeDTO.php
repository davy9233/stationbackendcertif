<?php

declare(strict_types=1);

namespace Domain\Station\DTO;

use Assert\Assert;
use Assert\LazyAssertion;
use Shared\Domain\DTO\ValidatorTrait;

final class CityInseeDTO
{
    use ValidatorTrait;

    public function __construct(
        private readonly string $name,
        private readonly string $insee,
        private readonly string $postal,
        private readonly string $codeRegion,
        private readonly string $codeDepartment,
    ) {
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getInsee(): ?string
    {
        return $this->insee;
    }

    public function getPostal(): ?string
    {
        return $this->postal;
    }


    final public function getcodeDepartment(): string
    {
        return $this->codeDepartment;
    }


    final public function getCodeRegion(): string
    {
        return $this->codeRegion;
    }
    private function assertRules(): LazyAssertion
    {
        $lazyAssert = Assert::lazy()->tryAll()
            ->that($this->name, 'name')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->insee, 'insee')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->postal, 'postal')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->codeDepartment, 'codeDepartment')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
            ->that($this->codeRegion, 'codeRegion')
            ->notBlank('errors.search_must_be_defined')
            ->string('errors.search_must_be_string')
        ;

        return $lazyAssert;
    }

}
