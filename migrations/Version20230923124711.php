<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230923124711 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE day_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hours_opening_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE prix_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE region_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE station_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE station_electric_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE station_service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE type_carbu_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE day (id INT NOT NULL, name VARCHAR(255) NOT NULL, id_day INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE hours_opening (id INT NOT NULL, station_id INT NOT NULL, day_id INT NOT NULL, open VARCHAR(255) DEFAULT NULL, close VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AAFA021B21BDB235 ON hours_opening (station_id)');
        $this->addSql('CREATE INDEX IDX_AAFA021B9C24126 ON hours_opening (day_id)');
        $this->addSql('CREATE TABLE prix (id INT NOT NULL, fuel_id INT NOT NULL, station_id INT NOT NULL, value DOUBLE PRECISION DEFAULT NULL, maj_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, traitement_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, value_update DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F7EFEA5E97C79677 ON prix (fuel_id)');
        $this->addSql('CREATE INDEX IDX_F7EFEA5E21BDB235 ON prix (station_id)');
        $this->addSql('CREATE TABLE region (id INT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE service (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE station (id INT NOT NULL, num VARCHAR(255) NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, zip_code VARCHAR(255) NOT NULL, insee_code VARCHAR(255) DEFAULT NULL, city VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, label_address VARCHAR(255) DEFAULT NULL, house_number VARCHAR(255) DEFAULT NULL, street VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, automate BOOLEAN NOT NULL, open TIME(0) WITHOUT TIME ZONE DEFAULT NULL, closed TIME(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE station_electric (id INT NOT NULL, address VARCHAR(255) NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE station_service (id INT NOT NULL, station_id INT DEFAULT NULL, service_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A83B3F8C21BDB235 ON station_service (station_id)');
        $this->addSql('CREATE INDEX IDX_A83B3F8CED5CA9E6 ON station_service (service_id)');
        $this->addSql('CREATE TABLE type_carbu (id INT NOT NULL, short_name VARCHAR(255) NOT NULL, num INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE hours_opening ADD CONSTRAINT FK_AAFA021B21BDB235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hours_opening ADD CONSTRAINT FK_AAFA021B9C24126 FOREIGN KEY (day_id) REFERENCES day (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E97C79677 FOREIGN KEY (fuel_id) REFERENCES type_carbu (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E21BDB235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE station_service ADD CONSTRAINT FK_A83B3F8C21BDB235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE station_service ADD CONSTRAINT FK_A83B3F8CED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE day_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hours_opening_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE prix_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE region_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE station_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE station_electric_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE station_service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE type_carbu_id_seq CASCADE');
        $this->addSql('ALTER TABLE hours_opening DROP CONSTRAINT FK_AAFA021B21BDB235');
        $this->addSql('ALTER TABLE hours_opening DROP CONSTRAINT FK_AAFA021B9C24126');
        $this->addSql('ALTER TABLE prix DROP CONSTRAINT FK_F7EFEA5E97C79677');
        $this->addSql('ALTER TABLE prix DROP CONSTRAINT FK_F7EFEA5E21BDB235');
        $this->addSql('ALTER TABLE station_service DROP CONSTRAINT FK_A83B3F8C21BDB235');
        $this->addSql('ALTER TABLE station_service DROP CONSTRAINT FK_A83B3F8CED5CA9E6');
        $this->addSql('DROP TABLE day');
        $this->addSql('DROP TABLE hours_opening');
        $this->addSql('DROP TABLE prix');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE station');
        $this->addSql('DROP TABLE station_electric');
        $this->addSql('DROP TABLE station_service');
        $this->addSql('DROP TABLE type_carbu');
    }
}
