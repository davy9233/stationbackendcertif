<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230924063450 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE etab_insee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE etab_insee (id INT NOT NULL, name_unit_legal VARCHAR(255) NOT NULL, complement_adresse_etablissement VARCHAR(255) DEFAULT NULL, numero_voie_etablissement VARCHAR(255) DEFAULT NULL, type_voie_etablissement VARCHAR(255) DEFAULT NULL, libelle_voie_etablissement VARCHAR(255) NOT NULL, code_postal_etablissement VARCHAR(255) NOT NULL, libelle_commune_etablissement VARCHAR(255) NOT NULL, enseigne1_etablissement VARCHAR(255) DEFAULT NULL, enseigne2_etablissement VARCHAR(255) DEFAULT NULL, enseigne3_etablissement VARCHAR(255) DEFAULT NULL, denomination_usuelle_etablissement VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE etab_insee_id_seq CASCADE');
        $this->addSql('DROP TABLE etab_insee');
    }
}
