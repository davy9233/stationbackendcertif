<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230925082619 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE prix_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE price_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE price (id INT NOT NULL, fuel_id INT NOT NULL, station_id INT NOT NULL, value DOUBLE PRECISION DEFAULT NULL, maj_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, traitement_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, value_update DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CAC822D997C79677 ON price (fuel_id)');
        $this->addSql('CREATE INDEX IDX_CAC822D921BDB235 ON price (station_id)');
        $this->addSql('ALTER TABLE price ADD CONSTRAINT FK_CAC822D997C79677 FOREIGN KEY (fuel_id) REFERENCES type_carbu (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE price ADD CONSTRAINT FK_CAC822D921BDB235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE prix DROP CONSTRAINT fk_f7efea5e97c79677');
        $this->addSql('ALTER TABLE prix DROP CONSTRAINT fk_f7efea5e21bdb235');
        $this->addSql('DROP TABLE prix');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE price_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE prix_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE prix (id INT NOT NULL, fuel_id INT NOT NULL, station_id INT NOT NULL, value DOUBLE PRECISION DEFAULT NULL, maj_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, traitement_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, value_update DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_f7efea5e21bdb235 ON prix (station_id)');
        $this->addSql('CREATE INDEX idx_f7efea5e97c79677 ON prix (fuel_id)');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT fk_f7efea5e97c79677 FOREIGN KEY (fuel_id) REFERENCES type_carbu (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT fk_f7efea5e21bdb235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE price DROP CONSTRAINT FK_CAC822D997C79677');
        $this->addSql('ALTER TABLE price DROP CONSTRAINT FK_CAC822D921BDB235');
        $this->addSql('DROP TABLE price');
    }
}
