<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230924075806 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etab_insee ADD label_address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE etab_insee ADD house_number VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE etab_insee ADD street VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE etab_insee ADD name_format VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE etab_insee DROP label_address');
        $this->addSql('ALTER TABLE etab_insee DROP house_number');
        $this->addSql('ALTER TABLE etab_insee DROP street');
        $this->addSql('ALTER TABLE etab_insee DROP name_format');
    }
}
