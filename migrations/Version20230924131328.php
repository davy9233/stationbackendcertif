<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230924131328 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE station_insee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE station_insee (id INT NOT NULL, station_id INT DEFAULT NULL, etab_insee_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A29AC11B21BDB235 ON station_insee (station_id)');
        $this->addSql('CREATE INDEX IDX_A29AC11B8FF6963F ON station_insee (etab_insee_id)');
        $this->addSql('ALTER TABLE station_insee ADD CONSTRAINT FK_A29AC11B21BDB235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE station_insee ADD CONSTRAINT FK_A29AC11B8FF6963F FOREIGN KEY (etab_insee_id) REFERENCES etab_insee (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE station_insee_id_seq CASCADE');
        $this->addSql('ALTER TABLE station_insee DROP CONSTRAINT FK_A29AC11B21BDB235');
        $this->addSql('ALTER TABLE station_insee DROP CONSTRAINT FK_A29AC11B8FF6963F');
        $this->addSql('DROP TABLE station_insee');
    }
}
